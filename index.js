import mockObject from './src/MockObjectBuilder';
export default mockObject;

import upgradedMockFactory from './src/UpgradedMockFactory';
export { upgradedMockFactory };

export * from './src/Matchers';