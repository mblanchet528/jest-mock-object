[![pipeline status](https://gitlab.com/mblanchet528/jest-mock-object/badges/master/pipeline.svg)](https://gitlab.com/mblanchet528/jest-mock-object/commits/master)

# jest-mock-object

Jest-mock-object allows to mock an entire object. It will mock each of the object's functions using `jest.fn()`. As it is a layer over Jest's mocking framework, you still benefit from all Jest's mocks features and flexibility. It also allows to return values depending on the parameters used to invoke the mocked function.

## Installation

Since `jest-mock-object` uses Jest, you have to install Jest first: `npm install --save-dev jest`.

Then simply run `npm install --save-dev jest-mock-object`.

## Configuration

Jest may complain about `unexpected token import in MockObject`. If this is the case, add the following to your Jest's configuration: 
```
"transformIgnorePatterns": [
      "<rootDir>/node_modules/(?!jest-mock-object)"
    ]
```

## Usage

### Import

`import mockObject from 'jest-mock-object';`

### Examples

Suppose we have the following class:

```
class SimpleCalculator {
    
    add(a, b) {
        // function's code
    }
    
    substract(a, b) {
        // function's code
    }
}
``` 

#### Mocking an object based on its type

`let mock = mockObject().fromType(SimpleCalculator);`

#### Mocking an object based on its prototype

`let mock = mockObject().fromPrototype(SimpleCalculator.prototype);`

#### Mocking an object using a list of functions names

`let mock = mockObject().fromFunctionNames('add', 'substract');`

#### Accessing mocked functions

`mock.add` 

`mock.substract` 

## Mock factories

The `mockObject` function returns a builder. `fromType`, `fromPrototype` and `fromFunctionNames` are terminal functions. By default, all the mock object's functions will be created using `jest.fn()` so it will return a standart jest's mock. However, if you want to return you own mock implementation, you can use the builder's function `usingFactory(factory)` where `factory` is a function that returns your mock implementation. The factory will be applied to each mocked functions.

### Examples

``` 
let factory = () => jest.fn(() => 'foo bar');
let mock = mockObject().usingFactory(factory).fromPrototoype(SimpleCalculator.prototype);
```

### Built-in factory

Jest-mock-object supplies a mock factory that creates an upgraded version with additional features of jest's standard mock. Here's how to import it:

``` 
import { upgradedMockFactory } from 'jest-mock-object';
``` 

## `upgradedMockFactory`

### Mocking a mocked function's return value based on arguments

In order to return a value based on the arguments values, you can use

`mock.add.mockReturnValueWhenCalledWith(3, 1, 2)`

The code above will return `3` when `mock.add(1, 2)` is called. If other arguments are passed, `undefined` will be returned. Please note that `mock.mockReturnValue(...)` is priortized over `mock.mockReturnValueWhenCalledWith(...)`. This means that this code:
``` 
mock.add.mockReturnValue(4);
mock.add.mockReturnValueWhenCalledWith(3, 1, 2);
mock.add(1, 2);
``` 
will return `4`.

#### Argument matchers

##### Importing argument matchers

`import { any } from 'jest-mock-object';`

##### Using argument matchers

As of now, there are two supported argument matchers for `mockReturnValueWhenCalledWith`:
- The default matcher is the equality matcher. To use it, simply pass the expected argument value to `mockReturnValueWhenCalledWith` like this: `mock.add.mockReturnValueWhenCalledWith(3, 1, 2)`.
- `any` matches any value. However, arguments count must be the same.

More matchers will come. Feel free to contribute and submit a Merge Request to add new ones or file an issue with your suggestions!

##### Extending argument matchers

You can define your own custom argument matchers. For example, if we want to return a precise value only if an even number is passed, we can do: 
``` 
let isEven = (x) => ( (x % 2) === 0 );
mock.add.mockReturnValueWhenCalledWith(3, isEven, isEven);
``` 
