export default function mockObject() {
  return new MockObjectBuilder();
}

class MockObjectBuilder {

  constructor() {
    this._factoryMethod = () => jest.fn();
  }

  usingFactory(factory) {
    this._factoryMethod = factory;
    return this;
  }

  fromType(type) {
    if (!this._isFunction(type)) {
      throw new Error('Supplied argument is not a valid type constructor.');
    }
    return this.fromPrototype(type.prototype);
  }
  
  fromPrototype(prototype) {
    if (typeof prototype !== 'object') {
      throw new Error('Supplied argument is not a valid prototype.');
    }

    let functions = Object.getOwnPropertyNames(prototype)
      .filter((p) => (this._isFunction(prototype[p])))
      .filter((p) => (p !== 'constructor'));
    return this.fromFunctionNames(...functions);
  }

  _isFunction(property) {
    return typeof property === 'function';
  }

  fromFunctionNames(...functionNames) {
    let mock = {};
    functionNames.forEach((fn) => mock[fn] = this._factoryMethod());
    return mock;
  }
}