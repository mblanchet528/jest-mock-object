export default function upgradedMockFactory() {
  let fn = jest.fn();
  fn.mock.registeredCalls = [];
  fn.mockImplementation(tryMatchRegisteredCalls.bind(fn.mock));
  fn.mockReturnValueWhenCalledWith = registerCall.bind(fn.mock);
  return fn;
}

function tryMatchRegisteredCalls() {
  const lastCall = this.calls[this.calls.length - 1];
  const matchingCalls = this.registeredCalls.filter((call) => call.areArgumentsMatching(lastCall));
  return matchingCalls.length > 0 ? matchingCalls[0].getReturnValue() : undefined;
}

function registerCall(returnValue, ...args) {
  const matchingCallIndex = this.registeredCalls.findIndex((call) => call.areArgumentsEqual(args));
  if (matchingCallIndex === -1) {
    this.registeredCalls.push(new Call(args, returnValue));
  } else {
    this.registeredCalls[matchingCallIndex] = new Call(args, returnValue);
  }
}

class Call {
  constructor(args, returnValue) {
    this._args = args;
    this._returnValue = returnValue;
  }

  areArgumentsEqual(suppliedArgs) {
    return this._args.every((arg, index) => arg === suppliedArgs[index]);
  }

  areArgumentsMatching(suppliedArgs) {
    if (this._args.length !== suppliedArgs.length) {
      return false;
    }
    return this.areArgumentsEqual(suppliedArgs) || this._areArgumentsSatisfyingMatchers(suppliedArgs);
  }

  _areArgumentsSatisfyingMatchers(suppliedArgs) {
    return this._args.every((arg, index) => this._tryMatchArguments(arg, suppliedArgs[index]));
  }

  _tryMatchArguments(registeredArg, suppliedArg) {
    return this._isMatcher(registeredArg) && registeredArg(suppliedArg);
  }

  _isMatcher(obj) {
    return typeof obj === 'function';
  }

  getReturnValue() {
    return this._returnValue;
  }
}