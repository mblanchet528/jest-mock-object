import mockObject from './../index';
import { any } from './../index';
import { upgradedMockFactory } from './../index';

test('mockObject is defined', () => {
  expect(mockObject).toBeDefined();
})

test('can access matchers', () => {
  expect(any).toBeDefined();
})

test('upgradedMockFactory is defined', () => {
  expect(upgradedMockFactory).toBeDefined();
})