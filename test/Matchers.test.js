import { any } from 'Matchers';

test('given any value when using any matcher then return true', () => {
  expect(any()).toEqual(true);
})
