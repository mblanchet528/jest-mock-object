import mockObject from 'MockObjectBuilder';

test('given a type when I create a mock using its prototype then each functions are mocked', () => {
  const mock = mockObject().fromPrototype(TestClass.prototype);

  expect(mock.setX._isMockFunction).toEqual(true);
  expect(mock.getY._isMockFunction).toEqual(true);
})

test('given a type when I create a mock using its prototype then dont mock constructor', () => {
  const mock = mockObject().fromPrototype(TestClass.prototype);
  expect(mock.constructor.mock).toBeUndefined();    
})

test('when I create a mock using an invalid prototype then throw error', () => {
  expect(() => mockObject().fromPrototype(TestClass)).toThrow('Supplied argument is not a valid prototype.');
})

test('given a type when I create a mock using its type then each functions are mocked', () => {
  const mock = mockObject().fromType(TestClass);

  expect(mock.setX._isMockFunction).toEqual(true);
  expect(mock.getY._isMockFunction).toEqual(true);
})

test('given a type when I create a mock using its type then dont mock constructor', () => {
  const mock = mockObject().fromType(TestClass);
  expect(mock.constructor.mock).toBeUndefined();
})

test('when I create a mock using an invalid type then throw error', () => {
  expect(() => mockObject().fromType({})).toThrow('Supplied argument is not a valid type constructor.');
})

test('when I create a mock from function names then create a mock having specified mocked functions', () => {
  const mock = mockObject().fromFunctionNames('execute', 'cancel');

  expect(mock.execute._isMockFunction).toEqual(true);
  expect(mock.cancel._isMockFunction).toEqual(true);
})

test('given I supply no custom mock factory when I create a mock from function names then create standard jest mock', () => {
  const mock = mockObject().fromFunctionNames('execute');
  expect(mock.execute._isMockFunction).toEqual(true);
})

test('given I supply my own custom mock factory when I create a mock from function names then use custom factory', () => {
  const factory = jest.fn(() => jest.fn());
  const mock = mockObject().usingFactory(factory).fromFunctionNames('execute', 'cancel');
  expect(factory).toHaveBeenCalledTimes(2);
})

test('given I supply no custom mock factory when I create a mock from prototype then use create standard jest mock', () => {
  const mock = mockObject().fromPrototype(TestClass.prototype);
  expect(mock.getY._isMockFunction).toEqual(true);
})

test('given I supply my own custom mock factory when I create a mock from prototype then use custom factory', () => {
  const factory = jest.fn(() => jest.fn());
  const mock = mockObject().usingFactory(factory).fromPrototype(TestClass.prototype);
  expect(factory).toHaveBeenCalledTimes(2);
})

class TestClass {

  constructor(x, y) {
    this._x = x;
    this._y = y;
  }

  setX(x) {
    this._x = x;
  }

  getY() {
    return this._y;
  }
}