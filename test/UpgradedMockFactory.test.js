import upgradedMockFactory from 'UpgradedMockFactory';

const ARGUMENT_1 = 1;
const ARGUMENT_2 = 2;
const RETURN_VALUE_1 = 3;
const RETURN_VALUE_2 = 4;
const EVEN_NUMBER_MATCHER = (x) => ((x % 2) === 0);

let mock;

beforeEach(() => {
  mock = upgradedMockFactory();
})

test('when create upgraded mock then return a mock', () => {
  expect(mock._isMockFunction).toEqual(true);
})

test('when create upgraded mock then define mockReturnValueWhenCalledWith function', () => {
  expect(mock.mockReturnValueWhenCalledWith).toBeDefined();
  expect(typeof mock.mockReturnValueWhenCalledWith).toEqual('function');
})

test('given registered call with one argument when mock called with same argument then return mocked return value', () => {
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_1, ARGUMENT_1);
  const result = mock(ARGUMENT_1);
  expect(result).toEqual(RETURN_VALUE_1);
})

test('given registered call with one argument when mock called with different argument then return undefined', () => {
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_1, ARGUMENT_1);
  const result = mock(ARGUMENT_2);
  expect(result).toBeUndefined();
})

test('given registered call with many arguments when mock called with same arguments then return mocked return value', () => {
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_1, ARGUMENT_1, ARGUMENT_2);
  const result = mock(ARGUMENT_1, ARGUMENT_2);
  expect(result).toEqual(RETURN_VALUE_1);
})

test('given registered call with many arguments when mock called with different arguments then return undefined', () => {
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_1, ARGUMENT_1, ARGUMENT_2);
  const result = mock(ARGUMENT_2, ARGUMENT_1);
  expect(result).toBeUndefined();
})

test('given multiple registered call when mock called with registered call then return registered return value for this call', () => {
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_1, ARGUMENT_1, ARGUMENT_2);
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_2, ARGUMENT_2, ARGUMENT_1);

  const result = mock(ARGUMENT_2, ARGUMENT_1);

  expect(result).toEqual(RETURN_VALUE_2);
})

test('given registered call when register call with same arguments but different return value then return new return value', () => {
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_1, ARGUMENT_1);
  
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_2, ARGUMENT_1);
  const result = mock(ARGUMENT_1);

  expect(result).toEqual(RETURN_VALUE_2);
})

test('given a registered call with argument matcher when argument satisfy matcher then return registered return value', () => {
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_1, EVEN_NUMBER_MATCHER);
  const result = mock(2);
  expect(result).toEqual(RETURN_VALUE_1);
})

test('given many registered calls satisfy arguments when call mocked function then evaluate calls in registration order', () => {
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_1, EVEN_NUMBER_MATCHER);
  mock.mockReturnValueWhenCalledWith(RETURN_VALUE_2, ARGUMENT_2);

  const result = mock(ARGUMENT_2);

  expect(result).toEqual(RETURN_VALUE_1);
})
